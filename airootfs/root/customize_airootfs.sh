#!/bin/bash

function umaskFunc() {
    set -e -u
    umask 022
    }

function setTimeZoneAndClockFunc() {
    # Timezone
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    # Set clock to UTC
    hwclock --systohc --utc
    }

function configRootUserFunc() {
    usermod -s /usr/bin/bash root
    cp -aT /etc/skel/ /root/
    chmod 700 /root
    echo "root:password" | chpasswd
    }

function editOrCreateConfigFilesFunc () {

    # Locale
    echo "LANG=en_US.UTF-8" > /etc/locale.conf

    sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
    sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
    sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf
    }

function createLiveUserFunc () {
	# add liveuser
	useradd -m -p "liveuser" -u 500 -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash liveuser
    chown -R liveuser:users /home/liveuser
    echo "liveuser:password" | chpasswd

    #enable autologin
    groupadd -r autologin
    gpasswd -a liveuser autologin

	groupadd -r nopasswdlogin
	gpasswd -a liveuser nopasswdlogin
	}

function setDefaultsFunc() {
    export _EDITOR=nano
    echo "EDITOR=${_EDITOR}" >> /etc/environment
    echo "EDITOR=${_EDITOR}" >> /etc/skel/.bashrc
    echo "EDITOR=${_EDITOR}" >> /etc/profile
    }

function enableServicesFunc() {
	systemctl enable lightdm.service
	systemctl set-default graphical.target
	systemctl enable NetworkManager.service
	}

function fixHibernateFunc() {
    sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
    sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
    sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf
    }

function initkeysFunc() {
    pacman-key --init
    pacman-key --populate archlinux
    }


umaskFunc
echo "#####   Function umaskFunc done    #####"
#localeGenFunc
#echo "#####   Function localeGenFunc done    #####"
setTimeZoneAndClockFunc
echo "#####   Function setTimeZoneAndClockFunc done    #####"
editOrCreateConfigFilesFunc
echo "#####   Function editOrCreateConfigFilesFunc done    #####"
configRootUserFunc
echo "#####   Function configRootUserFunc done    #####"
createLiveUserFunc
echo "#####   Function createLiveUserFunc done    #####"
setDefaultsFunc
echo "#####   Function setDefaultsFunc done    #####"
enableServicesFunc
echo "#####   Function enableServicesFunc done    #####"
fixHibernateFunc
echo "#####   Function fixHibernateFunc done    #####"
initkeysFunc
echo "#####   Function  initkeysFunc done    #####"
