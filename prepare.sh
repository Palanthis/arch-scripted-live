#!/bin/bash
# Written by Palanthis
set -e
 
sudo chown root airootfs/etc/sudoers
sudo chown root airootfs/etc/sudoers.d
sudo chown root airootfs/etc/sudoers.d/*
sudo chmod +x airootfs/root/git-installs.sh
